class CreateJokes < ActiveRecord::Migration[6.0]
  def change
    create_table :jokes do |t|
      t.string :chuck_joke
      t.string :category

      t.index :chuck_joke, unique: true
      t.timestamps
    end
  end
end
