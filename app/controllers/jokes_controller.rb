require './app/workers/fetch_worker'
require 'sinatra/base'
require 'pagy'
require 'pagy/extras/bootstrap'
require 'pagy/extras/overflow'
require_all './app/views/jokes'

class JokesController < Application
  include Pagy::Backend
  Pagy::VARS[:overflow] = :last_page

  helpers do
    include Pagy::Frontend
  end

  post '/fetch' do
    FetchWorker.perform_async
  end

  get '/random_jokes' do
    @random_jokes = Joke.find(Joke.pluck(:id).sample(30))
    erb :'jokes/random_jokes'
  end

  get "/jokes" do
    @pagy, @jokes = pagy(search_by_text(params[:search_text]))
    erb :'jokes/index'
  end

  get "/jokes/:category" do
    @pagy, @jokes = pagy(search_by_category(params[:category]))
    erb :'jokes/index'
  end

  error Pagy::VariableError do
    erb :'errors/pagy_wrong_value'
  end

  private

  def search_by_text(text_param)
    text_param.blank? ? Joke.order(updated_at: :asc) : Joke.search_by_body(text_param)
  end

  def search_by_category(category_param)
    Joke.where(category: set_category(category_param))
  end

  def set_category(category_param)
    category_param == 'common' ? '' : category_param
  end

  def pagy_get_vars(collection, vars)
    {
      count: collection.count,
      page: params[:page],
      items: vars[:items] || 30
    }
  end
end
