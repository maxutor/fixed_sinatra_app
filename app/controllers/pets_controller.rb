require './application'
require_all './app/views/pets'

class PetsController < Application

  get '/pets' do
    @pets = Pet.all
    erb :'pets/index'
  end

  get '/pets/:id' do
    @pet = Pet.find(params[:id])
    erb :'pets/show'
  end

  delete '/pets/:id' do
    Pet.destroy(params[:id])
    erb :'pets/delete'
  end

  post '/pets' do
    @pet = Pet.create!(name: params[:name], age: params[:age])
    erb :'pets/show'
  end
end
