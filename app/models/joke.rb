require 'pg_search/tasks.rb'

class Joke < ActiveRecord::Base
  include PgSearch::Model

  validates_presence_of :chuck_joke

  COLORS = { nerdy: 'sandybrown', explicit: 'aquamarine'}

  pg_search_scope :search_by_body, against: :chuck_joke

  def color
    COLORS[category.to_sym] || 'moccasin'
  end
end
