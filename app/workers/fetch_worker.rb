require 'sidekiq'
require 'sidekiq-scheduler'
require './application'
require 'net/http'
require 'json'

class FetchWorker
  include Sidekiq::Worker

  sidekiq_options retry: false

  JOKE_API_URL = 'https://api.icndb.com/jokes/random/30'

  def perform
    response = get_response
    parsed_response = JSON.parse(response)

    jokes = get_jokes(parsed_response)
    Joke.upsert_all((jokes), unique_by: :chuck_joke)
  end

  private

  def get_response
    uri = URI(JOKE_API_URL)
    Net::HTTP.get(uri)
  end

  def get_jokes(parsed_response)
    parsed_response['value'].map do |joke_data|
      {
        chuck_joke: joke_data['joke'],
        category: (joke_data['categories'].first).to_s,
        updated_at: Time.now,
        created_at: Time.now
      }
    end
  end
end
