require 'sinatra'
require 'sinatra/activerecord'
require 'require_all'
require_all './app/workers'
require_all './app/models'
require_all './app/views/errors'
require_all './app/views/jokes'

class Application < Sinatra::Base
  register Sinatra::ActiveRecordExtension

  set :show_exceptions, false

  set :views, "app/views"

  error ActiveRecord::RecordNotFound do
    erb :'errors/cannot_find_by_id'
  end

  error ActiveRecord::RecordInvalid do
    erb :'errors/record_invalid'
  end
end
