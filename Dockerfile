FROM ruby:2.7.0

RUN apt-get update && apt-get install -qq -y build-essential

RUN mkdir /app
WORKDIR /app

RUN gem install bundler -v 2.1.4 --no-document
RUN bundle config git.allow_insecure true
COPY Gemfile ./
COPY Gemfile.lock ./

RUN bundle check || bundle install
COPY . /app

ENTRYPOINT ["bundle", "exec"]
