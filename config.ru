require 'rubygems'
require 'bundler'
require 'sidekiq/web'

Bundler.require

require './application'
require_all './app/controllers'
require_all './app/workers'

use PetsController
use JokesController

run Application
run Rack::URLMap.new('/' => Application, '/sidekiq' => Sidekiq::Web)
